using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrowthPill : MonoBehaviour
{
    public float timer = 0;
    public float growTime = 6f;
    public float maxSize = 2f;

    public bool isMaxSize = false;

    //The brain and programming to increase the size of the player gradually
    private IEnumerator Grow()
    {
        Vector2 startScale = transform.localScale;
        Vector2 maxScale = new Vector2(maxSize, maxSize);

        do
        {
            transform.localScale = Vector3.Lerp(startScale, maxScale, timer / growTime);
            timer += Time.deltaTime;
            yield return null;
        }
        while (timer < growTime);

        isMaxSize = true;

    }

    //This gets called once the purchace button of the item has been clicked, this then calls the Grow function
    public void GrowTime()
    {
        if (isMaxSize == false)
        {
            StartCoroutine(Grow());
        }
    }
}
