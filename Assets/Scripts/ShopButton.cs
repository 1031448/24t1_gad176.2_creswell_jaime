using SAE.GAD176.Project2;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopButton : MonoBehaviour
{
    [SerializeField] private Item itemToBuy;
    [SerializeField] private Inventory inventory;

    public void PurchaseItem()
    {
        Debug.Log("Purchased item");

        // Add the item to the players inventory
        // Need a reference to the inventory script
        inventory.items.Add(itemToBuy);

        // Also like to deduct coins
        inventory.SubtractCoins(itemToBuy.cost);
        inventory.UpdateUI();

        //To hide the button and make it so it cant be purchased
        //We can Deactivate the gameobject

       gameObject.SetActive(false);
    }
    


}
