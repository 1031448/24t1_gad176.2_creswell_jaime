using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private TextMeshProUGUI shopLabel;

    [SerializeField] public Button speed;
    void Start()
    {
        if (shopLabel != null)
        {
            shopLabel.enabled = false;
        }
    }

    // This is displayed once the shop has been opended
    public void ShopOpen()
    {

        shopLabel.enabled = true;
     
        if (shopLabel == true) 
        {
            shopLabel.text = "Welcome to the shop, You can spend your coins here on what ever you like as long as you have enough!";
           
        }

    }

    //this closes the shop UI and makes it disable once the "Close Shop" button has been clicked
    public void ShopClose()
    {
        if (shopLabel == true)
        {
            shopLabel.enabled = false;
        }
    }
}
