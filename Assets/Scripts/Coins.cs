using SAE.GAD176.Project2;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.Rendering.DebugUI;

public class Coins : MonoBehaviour
{
    [SerializeField] protected private int value;



    private void Start()
    {
        
    }

    //This makes it so the coins get destoryed when the player comes in contact
    //Also increases how many coins the player has both in the UI and Inventory
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject);
            Inventory.instance.IncreaseCoins(value);
        }
    }
}
