using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapeOfGravity1 : MonoBehaviour
{
    //public Transform Coin;

    public List<Transform> coins = new List<Transform>();

    //Rigidbody2D coinsBody;

    public float influenceRange;

    public float intensity;

    //public float distanceToPlayer;

    Vector2 pullForce;

    public bool isPulling = false;

    public GameObject Target;
    void Start()
    {
        isPulling = false;

        //coinsBody = Coin.GetComponent<Rigidbody2D>();
    }

    // The brains of aquring the location of both the player and the coins and the dragging the coin to the player once within a certain range
     public void Update()
    {
        if (isPulling == true)
        {
            for (int i = 0; i < coins.Count; i++)
            {
                if (Vector2.Distance(coins[i].position, transform.position) < influenceRange)
                {
                    pullForce = (transform.position - coins[i].position).normalized / Vector2.Distance(coins[i].position, transform.position * intensity);
                    coins[i].gameObject.GetComponent<Rigidbody2D>().AddForce(pullForce, ForceMode2D.Force);
                }
            }

        }

        transform.position = Vector2.MoveTowards(transform.position, Target.transform.position, 5 * Time.deltaTime);
    }

    private void OnTriggerEnter2D (Collider2D other)
    {
        //Check if its a coin and then add it to the coins list
        if (other.CompareTag("Coin"))
        {
            coins.Add(other.transform);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        //Check if its a coin and then remove from the coins list
        if (other.CompareTag("Coin"))
        {
            coins.Remove(other.transform);
        }
    }

    public void Pull()
    {
        isPulling = true;
    }
}
