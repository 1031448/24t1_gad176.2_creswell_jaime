using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace SAE.GAD176.Project2
{


    public class Inventory : MonoBehaviour
    {
        [SerializeField] private TMP_Text coinText;

        public List<Item> items = new List<Item>();

        [SerializeField] public int currentCoins = 0;

        public static Inventory instance;

        [SerializeField] private Button GrowthPill;
        [SerializeField] private Button SonicShoes;
        [SerializeField] private Button CapeOfGravity;
         
         void Awake()
        {
            instance = this;

        }

        void Start()
        {
            //Update coins and disables buttons from the start 
            coinText.text = "You have " + currentCoins.ToString() + " Coins";

            GrowthPill.enabled = false;
            SonicShoes.enabled = false;
            CapeOfGravity.enabled = false;
        }

        //This increases the coin amount every time you pick up a coin
        public void IncreaseCoins(int v)
        {
            currentCoins += v;
            UpdateUI();
        }
        //Updates the UI once the player has gained or spent coins
        public void UpdateUI()
        {
            coinText.text = "You have " + currentCoins.ToString() + " Coins";
        }
        //This works with the UI update to subtract coins whenever an item is purchased
        public void SubtractCoins(int amount)
        {
            currentCoins -= amount;
        }

        //This void update will be used to check if the player has enough Coins to purchase the items
        void Update()
        {
            if (currentCoins >= 10)
            {
                GrowthPill.enabled = true;
            }
            else 
            {
              GrowthPill.enabled = false;
            }

            if (currentCoins >= 15)
            {
                SonicShoes.enabled = true;
            }

            else
            {
                SonicShoes.enabled = false;

            }

            if (currentCoins >= 20)
            {
                CapeOfGravity.enabled = true;
            }

            else
            {
                CapeOfGravity.enabled = false;
            }

        }
    }
    
}
      